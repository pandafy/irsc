from django import forms
from captcha.fields import CaptchaField

class CaptchaTestForm(forms.Form):
       captcha = CaptchaField()
       name = forms.CharField(max_length=50)