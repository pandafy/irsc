from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name = 'home'),
	path('oncall',views.oncall,name = 'oncall'),
	path('membership',views.membership, name = "membership"),
	path('bikeform',views.bikeform,name='bikeForm'),
	path('carform',views.carform,name='carForm'),
	path('submit',views.oncall,name='submit')
]