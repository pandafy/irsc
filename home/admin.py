
from django.contrib import admin
from .models import bike,car
# Register your models here
# 
class BikeAdmin(admin.ModelAdmin):
	list_display = ('reg_no','model','year','city','isBike','bik')
	list_display_links = ('reg_no','model','city')
	search_fields = ('city','state','reg_no','model','year','f_name','l_name','postal','bik')
	list_per_page = 25

class CarAdmin(admin.ModelAdmin):
	list_display = ('reg_no','model','year','city','isBike','carr')
	list_display_links = ('reg_no','model','city')
	search_fields = ('city','state','reg_no','model','year','f_name','l_name','postal','carr')
	list_per_page = 25

admin.site.register(bike,BikeAdmin)
admin.site.register(car,CarAdmin)
