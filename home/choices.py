STATES = ( ("ASSM","Assam"),("DEL","Delhi"),("HR","Haryana"),("MAN","Manipur"),("PUN","Punjab"),("RAJ", "Rajasthan"))
BIKES = (("ACT","Activa"), ("ARTR","Apache RTR"),("DISC", "Discover"),("PLAT", "Platina"))
CAR = (("AC","Acent"),("POL","Polo"),("SW", "Swift"),("WR","Wagon R"))
YEARS = ((x,x) for x in range(2000,2019) )