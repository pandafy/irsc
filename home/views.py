from django.shortcuts import render,redirect
from .models import bike,car
from .choices import STATES,YEARS,BIKES,CAR
from django.contrib import messages


# Create your views here.
def home(request):
    return render(request,'home.html')

def oncall(request):
    return render(request,'oncall.html')

def membership(request):
    return render(request,'membership.html')

def bikeform(request):
    if request.method == 'POST':
        entry = bike(
            f_name=request.POST['f_name'],
            l_name=request.POST['l_name'],
            email=request.POST['email'],
            mob=int(request.POST['mob']),
            add=request.POST['add'],
            landmark = request.POST['landmark'],
            city = request.POST['city'],
            state = request.POST['state'],
            postal = int(request.POST['postal']),

            bik=request.POST['bikk'],
            make=request.POST['make'],
            model = request.POST['model'],
            year = int(request.POST['year']),
            reg_no = request.POST['reg_no'],
            insr_cmpny = request.POST['insr_cmpny'],
            insr_expt=request.POST['insr_expt'],
            isBike=True
            
        )
        entry.save()
        messages.success(request,'Your entry has been saved.')

    context = {
        'state' : STATES,
        'year' : YEARS,
        'carr' : CAR,
        'bikk' : BIKES
    }
    return render(request,'bikeform.html',context)

def carform(request):
    if request.method == 'POST':
        entry = car(
            f_name=request.POST['f_name'],
            l_name=request.POST['l_name'],
            email=request.POST['email'],
            mob=int(request.POST['mob']),
            add=request.POST['add'],
            landmark = request.POST['landmark'],
            city = request.POST['city'],
            state = request.POST['state'],
            postal = int(request.POST['postal']),

            carr=request.POST['carr'],
            make=request.POST['make'],
            model = request.POST['model'],
            year = int(request.POST['year']),
            reg_no = request.POST['reg_no'],
            insr_cmpny = request.POST['insr_cmpny'],
            insr_expt=request.POST['insr_expt'],
            isBike=False
            
        )
        entry.save()
        messages.success(request,'Your entry has been saved.')
    return render(request,'carform.html')