from django.db import models
from datetime import date
from .choices import STATES,YEARS,CAR,BIKES

# Create your models here.
class user_details(models.Model):
    f_name = models.CharField(max_length=50, blank = False)
    l_name = models.CharField(max_length=50, blank = False)
    email = models.EmailField(max_length=50, blank=False)
    mob = models.IntegerField( blank=False)
    add = models.TextField(blank=False)
    landmark = models.TextField()
    city = models.CharField(max_length=50)
    state = models.CharField(choices=STATES,blank = False,max_length=50)
    postal = models.IntegerField()
    
    make = models.CharField(max_length=20,blank = False)
    model = models.CharField(max_length =20, blank = False)
    year = models.IntegerField(choices=YEARS,blank = False)
    reg_no = models.CharField(max_length=10, blank =False)
    insr_cmpny = models.CharField(max_length=30, blank = False)
    insr_expt = models.CharField(max_length= 20,blank = False)
    isBike = models.BooleanField(blank=False)

class bike(user_details):
    bik = models.CharField(choices=BIKES,blank=False,max_length=50)
    
class car(user_details):
    carr = models.CharField(choices = CAR, blank = False,max_length=50)    

